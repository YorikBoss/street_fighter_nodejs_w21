const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// DONE: Implement route controllers for fighter

//--
router.post('/', createFighterValid, function (req, res, next) {
    if (res.locals.invalid === undefined || res.locals.invalid.length === 0) {
        const result = FighterService.saveInRepository(req.body);
        if (result) {
            res.locals.result = result;
        } else {
            res.locals.fighterExist = true;
        }
    }
    next();
}, responseMiddleware);
router.put('/:id', updateFighterValid, function (req, res, next) {
    if (res.locals.invalid === undefined || res.locals.invalid.length === 0) {
        const result = FighterService.updateInRepository(req.params["id"], req.body);
        if (result.id) {
            res.locals.result = result;
        } else {
            res.locals.fighterExist = false;
        }
    }
    next();
}, responseMiddleware);
router.delete('/:id', function (req, res, next) {
    const result = FighterService.deleteFromRepository(req.params["id"]);
    if (result) {
        res.locals.result = result;
    } else {
        res.locals.fighterExist = false;
    }
    next();
}, responseMiddleware);
router.get('/:id', function (req, res, next) {
    const result = FighterService.searchInRepository({ "id": req.params["id"] });
    if (result) {
        res.locals.result = result;
    } else {
        res.locals.fighterExist = false;
    }
    next();
}, responseMiddleware);
router.get('/', function (req, res, next) {
    const result = FighterService.getAllFromRepository();
    res.locals.result = result;
    next();
}, responseMiddleware);
//--

module.exports = router;