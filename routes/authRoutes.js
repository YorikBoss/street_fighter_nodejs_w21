const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // DONE: Implement login action (get the user if it exist with entered credentials)
        const result = AuthService.login(req.body);
        res.locals.result = result;
    } catch (err) {
        res.locals.errorAuth = err.message;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;