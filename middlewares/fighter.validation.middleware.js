const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // DONE: Implement validatior for fighter entity during creation
    //--
    const newFighter = req.body;
    res.locals.invalid = [];
    if (newFighter) {
        const stringMessage = "does not match the format";
        if (typeof newFighter.name !== typeof fighter.name) {
            res.locals.invalid.push(`name ${stringMessage}`);
        }
        if (typeof newFighter.power !== typeof fighter.power || newFighter.power >= 100) {
            res.locals.invalid.push(`power ${stringMessage}`);
        }
        if (typeof newFighter.defense !== typeof fighter.defense) {
            res.locals.invalid.push(`defense  ${stringMessage}`);
        }
    } else res.locals.invalid.push('Object newFighter is missing');
    //--
    next();
}

const updateFighterValid = (req, res, next) => {
    // DONE: Implement validatior for fighter entity during update
    //--
    const newFighter = req.body;
    res.locals.invalid = [];
    if (newFighter) {
        const stringMessage = "does not match the format";
        if (newFighter.name !== undefined && typeof newFighter.name !== typeof fighter.name) {
            res.locals.invalid.push(`name ${stringMessage}`);
        }
        if (newFighter.power !== undefined && typeof newFighter.power !== typeof fighter.power || newFighter.power >= 100) {
            res.locals.invalid.push(`power ${stringMessage}`);
        }
        if (newFighter.defense !== undefined && typeof newFighter.defense !== typeof fighter.defense) {
            res.locals.invalid.push(`defense  ${stringMessage}`);
        }
    } else res.locals.invalid.push('Object newFighter is missing');
    //--
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;