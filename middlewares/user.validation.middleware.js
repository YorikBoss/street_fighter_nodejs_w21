const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // DONE: Implement validatior for user entity during creation
    //--
    const newUser = req.body;
    res.locals.invalid = [];
    if (newUser) {
        const stringMessage = "does not match the format";
        if (typeof newUser.firstName !== typeof user.firstName) {
            res.locals.invalid.push(`first name ${stringMessage}`);
        }
        if (typeof newUser.lastName !== typeof user.lastName) {
            res.locals.invalid.push(`last name ${stringMessage}`);
        }
        if (typeof newUser.email !== typeof user.email ||
            newUser.email.slice(newUser.email.length - 10, newUser.email.length) !== "@gmail.com") {
            res.locals.invalid.push(`email  ${stringMessage}`);
        }
        if (typeof newUser.phoneNumber !== typeof user.phoneNumber ||
            newUser.phoneNumber.slice(0, 4) !== "+380") {
            res.locals.invalid.push(`phone number  ${stringMessage}`);
        }
        if (typeof newUser.password !== typeof user.password ||
            newUser.password.length < 3) {
            res.locals.invalid.push(`password  ${stringMessage}`);
        }
        if (newUser.id) {
            res.locals.invalid.push(`Dont send ID into POST request`);
        }
        // todo: сохранить что-то лишнее
    } else res.locals.invalid.push('Object newUser is missing');
    //--
    next();
}

const updateUserValid = (req, res, next) => {
    // DONE: Implement validatior for user entity during update
    //--
    const updateUser = req.body;
    res.locals.invalid = [];
    if (updateUser) {
        const stringMessage = "does not match the format";
        if (updateUser.firstName !== undefined && typeof updateUser.firstName !== typeof user.firstName) {
            res.locals.invalid.push(`first name ${stringMessage}`);
        }
        if (updateUser.lastName !== undefined && typeof updateUser.lastName !== typeof user.lastName) {
            res.locals.invalid.push(`last name ${stringMessage}`);
        }
        if (updateUser.email !== undefined && (typeof updateUser.email !== typeof user.email ||
            updateUser.email.slice(updateUser.email.length - 10, updateUser.email.length) !== "@gmail.com")) {
            res.locals.invalid.push(`email  ${stringMessage}`);
        }
        if (updateUser.phoneNumber !== undefined && (typeof updateUser.phoneNumber !== typeof user.phoneNumber ||
            updateUser.phoneNumber.slice(0, 4) !== "+380")) {
            res.locals.invalid.push(`phone number  ${stringMessage}`);
        }
        if (updateUser.password !== undefined && (typeof updateUser.password !== typeof user.password ||
            updateUser.password.length < 3)) {
            res.locals.invalid.push(`password  ${stringMessage}`);
        }
    } else res.locals.invalid.push('Object updateUser is missing');
    //--
    next();
}


exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;