const responseMiddleware = (req, res, next) => {
    // DONE: Implement middleware that returns result of the query
    //--

    if (res.locals.invalid !== undefined && res.locals.invalid.length > 0) {
        message = res.locals.invalid.reduce((accumulator, currentValue) => {
            return accumulator + currentValue + "; ";
        }, "");
        res.status(400).send({
            error: true,
            message
        });
    } else if (res.locals.errorAuth) {
        res.status(404).send({
            error: true,
            message: res.locals.errorAuth
        });
    } else if (res.locals.result) {
        res.status(200).send(res.locals.result);
    } else {
        if (res.locals.userExist === true) {
            res.status(400).send({
                error: true,
                message: "this user already exists"
            });
        } else if (res.locals.userExist === false) {
            res.status(404).send({
                error: true,
                message: "this user not exists"
            });
        }
        if (res.locals.fighterExist === true) {
            res.status(400).send({
                error: true,
                message: "this fighter already exists"
            });
        } else if (res.locals.fighterExist === false) {
            res.status(404).send({
                error: true,
                message: "this fighter not exists"
            });
        }
    }
    //--
    // next();
}

exports.responseMiddleware = responseMiddleware;