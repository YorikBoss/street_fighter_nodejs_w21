const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // DONE: Implement methods to work with fighters

    //--
    saveInRepository(data){
        const result = this.searchInRepository({"name":data.name});
        if(!result){
            return FighterRepository.create({
                "name":data.name,
                "power":data.power,
                "defense":data.defense,
                "health":100
            });
        }
        return null;
    }
    updateInRepository(id, data){
        return FighterRepository.update(id, data);
    }
    deleteFromRepository(id){
        return FighterRepository.delete(id);
    }
    getAllFromRepository(){
        return FighterRepository.getAll();
    }
    searchInRepository(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    //--

}

module.exports = new FighterService();