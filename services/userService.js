const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // DONE: Implement methods to work with user

    //--
    saveInRepository(data) {
        const email = this.search({ "email": data.email });
        const phoneNumber = this.search({ "phoneNumber": data.phoneNumber });

        if (!email && !phoneNumber) {
            return UserRepository.create({
                "firstName": data.firstName,
                "lastName": data.lastName,
                "email": data.email,
                "phoneNumber": data.phoneNumber,
                "password": data.password
            });
        }
        return null;
    }
    updateInRepository(id, data) {
        return UserRepository.update(id, data);
    }
    deleteFromRepository(id) {
        return UserRepository.delete(id);
    }
    getAllFromRepository() {
        return UserRepository.getAll();
    }
    //--

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();